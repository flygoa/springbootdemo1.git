package com.pzr.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pzr.demo.staticclass.Properties;

/**
 * 启动类
 * @author ASUS
 *
 */
@SpringBootApplication
public class Appilcation {
	
	public static void main(String args[]){
        SpringApplication.run(Appilcation.class, args);
		System.out.println("使用端口："+Properties.port);
	}
}
