package com.pzr.demo.controller;

import java.util.Date;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pzr.demo.service.AsyncService;

/**
 * 异步请求 控制层
 * @author ASUS
 *
 */
@Controller
@RequestMapping("asyncController")
public class AsyncController {
	
	@Autowired
	AsyncService asyncService;
	
	/**
	 * 异步
	 * @return
	 */
	@RequestMapping("asyncTest")
	@ResponseBody
	public String asyncTest(){
		Date start = new Date();
		asyncService.task1();
		asyncService.task2();
		asyncService.task3();
		String resultStr = "主函数耗时："+((new Date()).getTime() - start.getTime());
		System.out.println(resultStr);
		return resultStr;
	}
	
	/**
	 * 先异步，后集体等待再执行下一步
	 * @return
	 */
	@RequestMapping("cyclicBarrierTest")
	@ResponseBody
	public String cyclicBarrierTest(){
		Date start = new Date();
		Future<String> task1 = asyncService.task1();
		Future<String> task2 = asyncService.task2();
		Future<String> task3 = asyncService.task3();
		while(true){
			if(task1.isDone() && task2.isDone() && task3.isDone()){
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		String resultStr = "主函数耗时："+((new Date()).getTime() - start.getTime());
		System.out.println(resultStr);
		return resultStr;
	}
	
	
	
	
}
