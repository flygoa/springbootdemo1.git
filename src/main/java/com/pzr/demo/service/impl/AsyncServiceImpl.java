package com.pzr.demo.service.impl;

import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.pzr.demo.service.AsyncService;

@Service
public class AsyncServiceImpl implements AsyncService {

	@Override
	@Async
	public Future<String> task1() {
		Future<String> result =null;
		try {
			Thread.sleep(10000);
			System.out.println("任务1：耗时10秒"+Thread.currentThread().getName());
			result =new AsyncResult<String>("任务1执行完毕"); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	@Async
	public Future<String> task2() {
		Future<String> result =null;
		try {
			Thread.sleep(5000);
			System.out.println("任务2：耗时5秒"+Thread.currentThread().getName());
			result =new AsyncResult<String>("任务2执行完毕"); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	@Async
	public Future<String> task3() {
		Future<String> result =null;
		try {
			Thread.sleep(15000);
			System.out.println("任务3：耗时15秒"+Thread.currentThread().getName());
			result =new AsyncResult<String>("任务3执行完毕"); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}

}
