package com.pzr.demo.service;

import java.util.concurrent.Future;

/**
 * 异步请求 业务层
 * @author ASUS
 *
 */
public interface AsyncService {
	
	public Future<String> task1();
	public Future<String> task2();
	public Future<String> task3();
	
}
