package com.pzr.demo.staticclass;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

	public static String port;

	
	@Value("${server.port}")
	public void setPort(String port) {
		Properties.port = port;
	}
	
}
